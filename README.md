#CMD for running app
  docker-compose up

#NOTE
  if twilio does not work. You can disable it by removing  @ValidPhoneNumber in dto/SignUpDto



#Register organization and admin user
- api : POST METHOD
    localhost:8080/api/v1/user/permit/signup
- body:
    {
      "nameOrg": "MyCompany",
      "phone": "+994555404544",
      "address": "Address",
      "username": "James",
      "email": "example@gmail.com",
      "password": "1234567"
    }



#Login as user
- api : POST METHOD
     localhost:8080/api/v1/user/permit/signin
- body:
   {
     "email": "example@gmail.com",
     "password": "1234567"
   }




#Create user with ADMIN role
- api : POST METHOD
    localhost:8080/api/v1/user/auth/create
- header:
    Bearer Token : jwt
- body:
    {
      "name": "name",
      "surname": "surname",
      "email": "name.example@gmail.com",
      "password": "12345678"
    }


#Create task
- api: POST METHOD
     localhost:8080/api/v1/task
- header:
    Bearer Token : jwt
- body:
     {
       "title": "task",
       "description": "task description",
       "deadline": "2022-10-07 22:00",
       "status": "pending"
     }


#Assign Task
- api: POST METHOD
     localhost:8080/api/v1/task/assign/task/1/user/1
- header:
    Bearer Token : jwt


#Get All Tasks
- api: GET METHOD
     localhost:8080/api/v1/task
- header:
    Bearer Token : jwt




