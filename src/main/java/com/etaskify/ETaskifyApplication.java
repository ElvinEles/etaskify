package com.etaskify;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class ETaskifyApplication {


    public static void main(String[] args) {
        SpringApplication.run(ETaskifyApplication.class, args);
    }


}
