package com.etaskify.service;

import com.etaskify.dto.TaskCreateDto;
import com.etaskify.dto.TaskDto;
import com.etaskify.dto.TaskDtoAll;
import com.etaskify.dto.UserAuth;
import com.etaskify.exception.OrganizationNotFound;
import com.etaskify.exception.TaskAlreadyExist;
import com.etaskify.exception.TaskNotFound;
import com.etaskify.exception.TimeNotCorrect;
import com.etaskify.model.Organization;
import com.etaskify.model.Task;
import com.etaskify.repository.OrganizationRepository;
import com.etaskify.repository.TaskRepository;
import com.etaskify.service.impl.TaskServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaskService implements TaskServiceImpl {

    private final TaskRepository taskRepository;
    private final UserService userService;

    private final OrganizationRepository organizationRepository;

    @Override
    public TaskDto createTask(TaskCreateDto dto) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(dto.deadline(), formatter);

        if (dateTime.isBefore(LocalDateTime.now())) {
            throw new TimeNotCorrect();
        }


        boolean taskByTitle = taskRepository.existsTaskByTitleIgnoreCase(dto.title().trim());

        if (taskByTitle) {
            throw new TaskAlreadyExist();
        }

        UserAuth principal = (UserAuth) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        Organization organization = organizationRepository.findById(principal.orgId())
                .orElseThrow(OrganizationNotFound::new);

        Task task = Task.builder()
                .title(dto.title().trim())
                .description(dto.description().trim())
                .status(dto.status())
                .deadline(dateTime)
                .organization(organization)
                .build();

        Task savedTask = taskRepository.save(task);


        return TaskDto.builder()
                .id(savedTask.getId())
                .description(savedTask.getDescription())
                .status(savedTask.getStatus())
                .title(savedTask.getTitle())
                .deadline(savedTask.getDeadline().toString())
                .build();

    }

    @Override
    public void assign(Long taskid, Long userid) {
        Task task = taskRepository.findById(taskid)
                .orElseThrow(TaskNotFound::new);
        userService.assignTask(task, userid);
    }

    @Override
    public List<TaskDtoAll> getAll() {

        UserAuth principal = (UserAuth) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        return taskRepository.findAllTaskWithUser(principal.id())
                .stream()
                .map(savedTask -> TaskDtoAll.builder()
                        .id(savedTask.getId())
                        .description(savedTask.getDescription())
                        .status(savedTask.getStatus())
                        .title(savedTask.getTitle())
                        .deadline(savedTask.getDeadline().toString())
                        .organization(savedTask.getOrganization())
                        .build())
                .toList();

    }
}
