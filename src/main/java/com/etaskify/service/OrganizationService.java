package com.etaskify.service;


import com.etaskify.exception.OrganizationExist;
import com.etaskify.model.Organization;
import com.etaskify.repository.OrganizationRepository;
import com.etaskify.service.impl.OrganizationServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrganizationService implements OrganizationServiceImpl {

    private final OrganizationRepository organizationRepository;


    @Override
    public Organization create(Organization model) {

        Optional<Organization> optionOrg = organizationRepository.findByName(model.getName());

        if (optionOrg.isPresent()) {
            throw new OrganizationExist();
        }

        return organizationRepository.save(model);
    }
}
