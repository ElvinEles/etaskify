package com.etaskify.service.impl;

import com.etaskify.model.Organization;

public interface OrganizationServiceImpl {

    Organization create(Organization dto);

}
