package com.etaskify.service.impl;

import com.etaskify.dto.*;

public interface UserServiceImpl {

    void signUp(SignUpDto dto);

    LoginResponseDto signIn(LoginRequestDto dto);

    UserDto createUser(RegisterDto dto);
}
