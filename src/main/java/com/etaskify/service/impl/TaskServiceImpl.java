package com.etaskify.service.impl;

import com.etaskify.dto.TaskCreateDto;
import com.etaskify.dto.TaskDto;
import com.etaskify.dto.TaskDtoAll;

import java.util.List;

public interface TaskServiceImpl {

    TaskDto createTask(TaskCreateDto dto);

    void assign(Long taskid, Long userid);

    List<TaskDtoAll> getAll();

}
