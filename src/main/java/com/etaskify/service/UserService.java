package com.etaskify.service;

import com.etaskify.dto.*;
import com.etaskify.exception.*;
import com.etaskify.mail.EmailService;
import com.etaskify.model.Organization;
import com.etaskify.model.Role;
import com.etaskify.model.Task;
import com.etaskify.model.User;
import com.etaskify.repository.OrganizationRepository;
import com.etaskify.repository.UserRepository;
import com.etaskify.security.jwt.JwtCredentials;
import com.etaskify.security.jwt.JwtService;
import com.etaskify.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserServiceImpl {

    private final UserRepository userRepository;
    private final OrganizationService organizationService;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsChecker userDetailsChecker;
    private final JwtService jwtService;
    private final EmailService emailService;
    private final OrganizationRepository organizationRepository;



    @Override
    @Transactional
    public void signUp(SignUpDto dto) {

        Optional<User> userDb = userRepository.findByEmail(dto.email());

        if (userDb.isPresent()) {
            throw new UserExistByEmail(dto.email());
        }


        Organization organizationModel = Organization.builder()
                .name(dto.nameOrg())
                .phone(dto.phone())
                .address(dto.address())
                .build();

        Organization savedOrg = organizationService.create(organizationModel);


        User user = User.builder()
                .name(dto.username())
                .email(dto.email())
                .password(passwordEncoder.encode(dto.password()))
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .accountNonExpired(true)
                .role(Role.ADMIN)
                .organization(savedOrg)
                .build();

        userRepository.save(user);
    }

    @Override
    public LoginResponseDto signIn(LoginRequestDto loginRequestDto) {
        User userDb = userRepository.findByEmail(loginRequestDto.email())
                .orElseThrow(() -> new UsernameNotFoundException(loginRequestDto.email()));


        boolean matches = passwordEncoder.matches(loginRequestDto.password(), userDb.getPassword());

        if (!matches) {
            throw new PasswordNotMatches();
        }

        userDetailsChecker.check(userDb);

        return LoginResponseDto.builder()
                .jwt(jwtService.generateToken(JwtCredentials.builder()
                        .id(userDb.getId())
                        .email(userDb.getEmail())
                        .role(userDb.getRole().name())
                        .orgId(userDb.getOrganization().getId())
                        .build()))
                .build();

    }

    @Override
    public UserDto createUser(RegisterDto dto) {
        Optional<User> userDb = userRepository.findByEmail(dto.email().trim());

        if (userDb.isPresent()) {
            throw new UserExistByEmail(dto.email());
        }

        UserAuth principal = (UserAuth) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        Organization organization = organizationRepository.findById(principal.orgId())
                .orElseThrow(OrganizationNotFound::new);


        User user = User.builder()
                .name(dto.name())
                .email(dto.email())
                .surname(dto.surname())
                .password(passwordEncoder.encode(dto.password()))
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .accountNonExpired(true)
                .role(Role.USER)
                .organization(organization)
                .build();

        User savedUser = userRepository.save(user);

        log.info("user {}", user);

        return UserDto.builder()
                .id(savedUser.getId())
                .name(savedUser.getName())
                .email(savedUser.getEmail())
                .surname(savedUser.getSurname())
                .organization(savedUser.getOrganization())
                .build();


    }


    public void assignTask(Task task, Long userid) {
        log.info("user query started");
        User user = userRepository.findById(userid)
                .orElseThrow(() -> new UsernameNotFoundException("id"));
        log.info("user query ended");
        if (!Objects.equals(task.getOrganization().getId(), user.getOrganization().getId())) {
            throw new OrganizationRelationshipException();
        }



        userRepository.assiginTask(task.getId(), userid);

        emailService.sendMail(user.getEmail(), task.getTitle());
    }
}
