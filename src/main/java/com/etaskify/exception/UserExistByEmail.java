package com.etaskify.exception;

public class UserExistByEmail extends RuntimeException {

    public UserExistByEmail(String message) {
        super(message);
    }
}
