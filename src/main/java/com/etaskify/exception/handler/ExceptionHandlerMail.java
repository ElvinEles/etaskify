package com.etaskify.exception.handler;

import com.etaskify.exception.MessageNotSend;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerMail {

    @ExceptionHandler(MessageNotSend.class)
    public ResponseEntity<Map<String, Object>> roleNotFound(MessageNotSend exception) {
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY)
                .body(mapErrors());
    }

    private Map<String, Object> mapErrors() {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", 502);
        errors.put("error", "the message could not be sent");
        return errors;
    }


}
