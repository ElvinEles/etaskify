package com.etaskify.exception.handler;

import com.etaskify.exception.OrganizationExist;
import com.etaskify.exception.OrganizationNotFound;
import com.etaskify.exception.OrganizationRelationshipException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerOrganization {

    @ExceptionHandler(OrganizationNotFound.class)
    public ResponseEntity<Map<String, Object>> orgNotExist(OrganizationNotFound exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors("organization is not found"));
    }

    @ExceptionHandler(OrganizationRelationshipException.class)
    public ResponseEntity<Map<String, Object>> organizationRelation(OrganizationRelationshipException exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors("task and user do not belong to the same organization"));
    }

    @ExceptionHandler(OrganizationExist.class)
    public ResponseEntity<Map<String, Object>> orgExist(OrganizationExist exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors("organization already exist"));
    }


    private Map<String, Object> mapErrors(String message) {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", 400);
        errors.put("error", message);
        return errors;
    }
}
