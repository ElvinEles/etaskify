package com.etaskify.exception.handler;

import com.etaskify.exception.PasswordNotMatches;
import com.etaskify.exception.PhoneNumberException;
import com.etaskify.exception.TimeNotCorrect;
import com.etaskify.exception.UserExistByEmail;
import com.twilio.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerCustom {

    @ExceptionHandler(PhoneNumberException.class)
    public ResponseEntity<Map<String, Object>> phoneNumber(PhoneNumberException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(mapErrors("phone number must not be null", 400));
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<Map<String, Object>> responseApiException(ApiException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(mapErrors("the phone number is not correct", 400));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, Object>> methodArgumentException(MethodArgumentNotValidException exception) {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", 400);

        for (FieldError error : exception.getFieldErrors()) {
            errors.put(error.getField(), error.getDefaultMessage());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
    }


    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Map<String, Object>> userNotFound(UsernameNotFoundException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(mapErrors("user is not found", 400));
    }


    @ExceptionHandler(PasswordNotMatches.class)
    public ResponseEntity<Map<String, Object>> passwordNotMatches(PasswordNotMatches exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(mapErrors("password is not matched", 401));
    }


    @ExceptionHandler(UserExistByEmail.class)
    public ResponseEntity<Map<String, Object>> userExist(UserExistByEmail exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors(exception.getMessage() + " exist", 409));
    }


    @ExceptionHandler(TimeNotCorrect.class)
    public ResponseEntity<Map<String, Object>> timeNotCorrect(TimeNotCorrect exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors("time cannot be in the past", 409));
    }


    private Map<String, Object> mapErrors(String message, Integer code) {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", code);
        errors.put("error", message);
        return errors;
    }

}
