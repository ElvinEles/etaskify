package com.etaskify.exception.handler;

import com.etaskify.exception.OrganizationNotFound;
import com.etaskify.exception.TaskAlreadyExist;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerTask {

    @ExceptionHandler(OrganizationNotFound.class)
    public ResponseEntity<Map<String, Object>> taskNotFound(OrganizationNotFound exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors("task is not found"));
    }

    @ExceptionHandler(TaskAlreadyExist.class)
    public ResponseEntity<Map<String, Object>> taskExist(TaskAlreadyExist exception) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(mapErrors("task already exist"));
    }


    private Map<String, Object> mapErrors(String message) {
        Map<String, Object> errors = new HashMap<>();
        errors.put("timestamp", Instant.now().toString());
        errors.put("status", 400);
        errors.put("error", message);
        return errors;
    }
}
