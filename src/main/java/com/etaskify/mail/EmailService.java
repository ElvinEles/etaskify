package com.etaskify.mail;


import com.etaskify.exception.MessageNotSend;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
@RequiredArgsConstructor
@Slf4j
public class EmailService {

    private final JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String senderEmail;


    public void sendMail(String sendToEmail, String task) {

        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");


        try {
            helper.setTo(sendToEmail);
            helper.setSubject("eTaskify - Assigned Task");
            helper.setText("Task: " + task + " is assigned to you");
            helper.setFrom(senderEmail);
            javaMailSender.send(mimeMessage);
            log.info("email is sent ");
        } catch (MessagingException | MailSendException exception) {
            throw new MessageNotSend();
        }
    }
}

