package com.etaskify.mapper;

import com.etaskify.dto.TaskDto;
import com.etaskify.dto.TaskDtoAll;
import com.etaskify.model.Task;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskMapper {

    TaskDto modelToDto(Task task);

    TaskDtoAll modelToDtoAll(Task task);
}
