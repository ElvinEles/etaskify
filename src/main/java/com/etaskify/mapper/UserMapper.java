package com.etaskify.mapper;

import com.etaskify.dto.UserDto;
import com.etaskify.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    UserDto modelToDto(User user);
}
