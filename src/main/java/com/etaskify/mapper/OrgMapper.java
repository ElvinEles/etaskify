package com.etaskify.mapper;

import com.etaskify.dto.OrganizationDto;
import com.etaskify.model.Organization;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrgMapper {

    OrganizationDto modelToDto(Organization organization);

    Organization dtoToModel(OrganizationDto organizationDto);
}
