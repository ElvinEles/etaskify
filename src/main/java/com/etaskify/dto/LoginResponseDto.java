package com.etaskify.dto;

import lombok.Builder;

@Builder
public record LoginResponseDto(
        String jwt
) {
}
