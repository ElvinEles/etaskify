package com.etaskify.dto;

import com.etaskify.model.Organization;
import lombok.Builder;

@Builder
public record UserDto(

        Long id,

        String name,

        String surname,

        String email,

        Organization organization
) {
}
