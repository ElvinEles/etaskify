package com.etaskify.dto;

import com.etaskify.model.Organization;
import lombok.Builder;

@Builder
public record TaskDtoAll(

        Long id,

        String title,

        String description,

        String status,

        String deadline,

        Organization organization

) {
}
