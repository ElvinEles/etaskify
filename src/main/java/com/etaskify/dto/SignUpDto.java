package com.etaskify.dto;

import com.etaskify.validation.annotation.ValidPhoneNumber;
import lombok.Builder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
public record SignUpDto(

        @NotBlank
        String nameOrg,

        @NotBlank
        @ValidPhoneNumber
        String phone,

        @NotBlank
        String address,

        @NotBlank
        String username,

        @NotBlank
        @Email
        String email,

        @NotBlank
        @Size(min = 6, message = "the password length should min 6")
        String password
) {
}
