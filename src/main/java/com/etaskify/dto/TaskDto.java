package com.etaskify.dto;

import lombok.Builder;

@Builder
public record TaskDto(

        Long id,

        String title,

        String description,

        String deadline,

        String status

) {


}
