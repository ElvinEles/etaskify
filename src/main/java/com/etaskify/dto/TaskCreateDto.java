package com.etaskify.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Builder
public record TaskCreateDto(

        @NotBlank
        String title,

        @NotBlank
        String description,

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm", shape = JsonFormat.Shape.STRING)
        @Pattern(regexp = "^(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2})$", message = "time form should be yyyy-MM-dd HH:mm")
        String deadline,

        @NotBlank
        String status

) {
}
