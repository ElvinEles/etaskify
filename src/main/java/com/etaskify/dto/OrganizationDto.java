package com.etaskify.dto;

import lombok.Builder;

import java.time.Instant;

@Builder
public record OrganizationDto(

        Long id,

        String name,

        String phone,

        String address,

        Instant createdAt,

        Instant updatedAt

) {
}
