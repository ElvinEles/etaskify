package com.etaskify.dto;

import lombok.Builder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
public record LoginRequestDto(

        @NotBlank
        @Email(message = "email is not correct")
        String email,

        @NotBlank
        @Size(min = 6, message = "the password length should min 6")
        String password

) {
}
