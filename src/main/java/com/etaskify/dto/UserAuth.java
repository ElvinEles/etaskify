package com.etaskify.dto;

import lombok.Builder;

@Builder
public record UserAuth(
        Long id,
        String email,
        Long orgId
) {
}

