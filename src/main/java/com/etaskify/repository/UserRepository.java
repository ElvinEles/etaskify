package com.etaskify.repository;

import com.etaskify.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @EntityGraph(value = "User.withRoles", type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"organization"})
    Optional<User> findByEmail(String email);

    @Override
    @EntityGraph(value = "User.withTask", type = EntityGraph.EntityGraphType.FETCH, attributePaths = {"task"})
    Optional<User> findById(Long aLong);

    @Transactional
    @Modifying
    @Query(value = "UPDATE User u SET u.task.id = :taskid WHERE u.id = :userid")
    int assiginTask(@Param("taskid") Long taskid, @Param("userid") Long userid);

}
