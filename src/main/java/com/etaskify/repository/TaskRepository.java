package com.etaskify.repository;

import com.etaskify.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    boolean existsTaskByTitleIgnoreCase(String title);

    @Query(value = "SELECT t FROM Task t INNER JOIN Organization o ON t.organization.id=o.id WHERE t.organization.id = :id")
    List<Task> findAllTaskWithUser(@Param("id") Long id);


}

