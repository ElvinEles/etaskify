package com.etaskify.validation.execution;

import com.etaskify.validation.annotation.NotEmptyLong;
import org.springframework.beans.factory.annotation.Configurable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Configurable
public class LongTypeValidator implements ConstraintValidator<NotEmptyLong, Long> {


    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return value != null;
    }
}


