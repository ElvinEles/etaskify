package com.etaskify.validation.execution;

import com.etaskify.exception.PhoneNumberException;
import com.etaskify.validation.annotation.ValidPhoneNumber;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.lookups.v1.PhoneNumber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Configurable
@Slf4j
public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {

    @Value("${twilio.account_sid}")
    private String accountSid;

    @Value("${twilio.auth_token}")
    private String authToken;


    @Override
    public void initialize(ValidPhoneNumber constraintAnnotation) {
        Twilio.init(accountSid, authToken);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value.trim().isEmpty()) {
            log.info("phone number is {}", value);
            throw new PhoneNumberException();
        }

        value = value.replaceAll("[\\s()-]", "");

        try {
            PhoneNumber.fetcher(new com.twilio.type.PhoneNumber(value)).fetch();
            return true;
        } catch (ApiException e) {
            if (e.getCode() == 404) {
                return false;
            }
            throw e;
        }
    }
}


