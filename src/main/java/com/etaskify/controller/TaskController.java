package com.etaskify.controller;

import com.etaskify.dto.TaskCreateDto;
import com.etaskify.dto.TaskDto;
import com.etaskify.dto.TaskDtoAll;
import com.etaskify.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/task")
@Slf4j
public class TaskController {

    private final TaskService taskService;


    @PostMapping
    public ResponseEntity<TaskDto> createTask(@Valid @RequestBody TaskCreateDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(taskService.createTask(dto));
    }

    @PostMapping("/assign/task/{taskid}/user/{userid}")
    public ResponseEntity<Void> assign(@PathVariable Long taskid, @PathVariable Long userid) {
        taskService.assign(taskid, userid);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping
    public ResponseEntity<List<TaskDtoAll>> getAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(taskService.getAll());
    }

}
