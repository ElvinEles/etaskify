package com.etaskify.controller;


import com.etaskify.dto.*;
import com.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;


    @PostMapping("/permit/signup")
    public ResponseEntity<Void> signUp(@Valid @RequestBody SignUpDto dto) {
        userService.signUp(dto);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/permit/signin")
    public ResponseEntity<LoginResponseDto> signIn(@Valid @RequestBody LoginRequestDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.signIn(dto));
    }

    @PostMapping("/auth/create")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody RegisterDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.createUser(dto));
    }
}
