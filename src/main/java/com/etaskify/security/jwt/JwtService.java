package com.etaskify.security.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

import static com.etaskify.security.jwt.JwtConstants.*;

@Service
public class JwtService {

    private Key key;

    @Value("${security.jwt.secret}")
    private String secret;

    @Value("${security.jwt.duration}")
    private long duration;

    @PostConstruct
    private void init() {
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        key = Keys.hmacShaKeyFor(keyBytes);
    }


    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


    public String generateToken(JwtCredentials jwtCredentials) {
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setIssuedAt(new Date())
                .claim(USER_ID, jwtCredentials.getId())
                .claim(USER_EMAIL, jwtCredentials.getEmail())
                .claim(USER_ROLE, jwtCredentials.getRole())
                .claim(USER_ORG, jwtCredentials.getOrgId())
                .setExpiration(Date.from(Instant.now().plusSeconds(duration)))
                .setHeader(Map.of(Header.TYPE, Header.JWT_TYPE))
                .signWith(key, SignatureAlgorithm.HS512);
        return jwtBuilder.compact();
    }
}

