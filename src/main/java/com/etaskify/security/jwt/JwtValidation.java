package com.etaskify.security.jwt;

import com.etaskify.dto.UserAuth;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.etaskify.security.jwt.HttpConstant.AUTH_HEADER;
import static com.etaskify.security.jwt.HttpConstant.BEARER_AUTH_HEADER;
import static com.etaskify.security.jwt.JwtConstants.*;


@Component
@Slf4j
@RequiredArgsConstructor
public class JwtValidation {

    private final JwtService jwtService;


    public Optional<Authentication> authentication(HttpServletRequest request) {
        return Optional.ofNullable(getAuthHeader(request))
                .flatMap(this::getAuthentication);
    }

    private Optional<Authentication> getAuthentication(String token) {
        if (token == null) {
            log.info("token is empty");
            return Optional.empty();
        }

        Claims claims = jwtService.parseToken(token.substring(BEARER_AUTH_HEADER.length()));

        if (claims.getExpiration().before(new Date())) {
            log.info("jwt exp date is expired");
            return Optional.empty();
        }

        return Optional.of(authenticate(claims));
    }

    private Authentication authenticate(Claims claims) {
        final List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(claims.get(USER_ROLE, String.class)));

        UserAuth userDetails = UserAuth.builder()
                .id(Long.valueOf(claims.get(USER_ID).toString()))
                .email(claims.get(USER_EMAIL).toString())
                .orgId(Long.valueOf(claims.get(USER_ORG).toString()))
                .build();
        log.info("userin role {}", authorities);

        return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
    }

    public String getAuthHeader(HttpServletRequest request) {
        if (request.getHeader(AUTH_HEADER) != null && request.getHeader(AUTH_HEADER)
                .toLowerCase()
                .startsWith(BEARER_AUTH_HEADER.toLowerCase())) {
            return request.getHeader(AUTH_HEADER);
        }
        log.warn("auth header is missing");
        return null;
    }

}

