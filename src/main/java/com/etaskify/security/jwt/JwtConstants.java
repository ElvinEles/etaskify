package com.etaskify.security.jwt;

public final class JwtConstants {

    public static final String USER_ID = "id";
    public static final String USER_EMAIL = "email";
    public static final String USER_ROLE = "role";
    public static final String USER_ORG = "org";

    private JwtConstants() {

    }
}
