package com.etaskify.security.jwt;

public final class HttpConstant {

    public static final String AUTH_HEADER = "Authorization";
    public static final String BEARER_AUTH_HEADER = "Bearer ";

    private HttpConstant() {
        throw new UnsupportedOperationException();
    }

}