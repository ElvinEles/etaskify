package com.etaskify.security;

import com.etaskify.model.Role;
import com.etaskify.security.jwt.JwtRequestFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration {

    private final JwtRequestFilter jwtRequestFilter;

    private static final String PERMIT = "/api/v1/user/permit/**";
    private static final String AUTH = "/api/v1/user/auth/**";
    private static final String TASK = "/api/v1/task/**";
    private static final String ACTUATOR = "/actuator/**";

    @Bean
    public SecurityFilterChain httpSecurity(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors().configurationSource(configurationSource());
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(new SecurityEntryPoint());
        http.exceptionHandling().accessDeniedHandler(new SecurityAccessDenied());
        http.authorizeRequests().antMatchers(ACTUATOR).permitAll();
        http.authorizeRequests().antMatchers(PERMIT).permitAll();
        http.authorizeRequests().antMatchers(AUTH).hasAnyAuthority(Role.ADMIN.name(), Role.USER.name());
        http.authorizeRequests().antMatchers(TASK).hasAnyAuthority(Role.ADMIN.name(), Role.USER.name());
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }

    private CorsConfigurationSource configurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.addAllowedHeader("*");
        config.addAllowedOrigin("*");
        config.addAllowedMethod("*");
        config.setAllowCredentials(true);
        source.registerCorsConfiguration("/**", config);
        return source;
    }


}
