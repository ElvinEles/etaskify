package com.etaskify.service;

import com.etaskify.dto.*;
import com.etaskify.exception.PasswordNotMatches;
import com.etaskify.exception.UserDetailsChecker;
import com.etaskify.mail.EmailService;
import com.etaskify.mapper.OrgMapper;
import com.etaskify.model.Organization;
import com.etaskify.model.Role;
import com.etaskify.model.Task;
import com.etaskify.model.User;
import com.etaskify.repository.OrganizationRepository;
import com.etaskify.repository.UserRepository;
import com.etaskify.security.jwt.JwtService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private OrganizationService organizationService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private UserRepository userRepository;
    @Mock
    private OrganizationRepository organizationRepository;
    @Mock
    private UserDetailsChecker userDetailsChecker;
    @Mock
    private JwtService jwtService;
    @Mock
    private EmailService emailService;

    @Mock
    private SecurityContext securityContext;
    @Mock
    private Authentication authentication;


    @Test
    void whenSignUpThenSuccess() {

        SignUpDto dto = new SignUpDto(
                "organization",
                "+994775404544",
                "Baku",
                "james",
                "eles.elvinn@gmail.com",
                "12345678"
        );

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());


        Organization organization = Organization.builder()
                .id(1L)
                .name(dto.nameOrg())
                .address(dto.address())
                .phone(dto.phone())
                .build();

        when(organizationService.create(any())).thenReturn(organization);

        User user = User.builder()
                .name(dto.username())
                .email(dto.email())
                .password(passwordEncoder.encode(dto.password()))
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .accountNonExpired(true)
                .role(Role.ADMIN)
                .organization(organization)
                .build();

        when(userRepository.save(any())).thenReturn(user);


        userService.signUp(dto);

        verify(userRepository, times(1)).save(any());
        verify(organizationService, times(1)).create(any());

    }

    @Test
    void whenSignInNotFoundUserThrowException() {


        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        LoginRequestDto requestDto = LoginRequestDto.builder()
                .email("wrongemail")
                .password("12345678")
                .build();

        assertThatThrownBy(() -> userService.signIn(requestDto))
                .isInstanceOf(UsernameNotFoundException.class);

    }

    @Test
    void whenSignInNotMatchPasswordThrowException() {


        User user = User.builder()
                .name("elvin")
                .email("eles")
                .password(passwordEncoder.encode("1234567"))
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .accountNonExpired(true)
                .role(Role.ADMIN)
                .build();


        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        when(passwordEncoder.matches(any(), any())).thenReturn(false);

        LoginRequestDto requestDto = LoginRequestDto.builder()
                .email("wrongemail")
                .password("12345678")
                .build();

        assertThatThrownBy(() -> userService.signIn(requestDto))
                .isInstanceOf(PasswordNotMatches.class);

    }

    @Test
    void whenSignInThenSuccess() {

        Organization organization = Organization.builder()
                .id(1L)
                .name("organization")
                .phone("+994558479995")
                .address("baku")
                .build();


        User user = User.builder()
                .id(1L)
                .name("elvin")
                .email("eles.elvin@gmail.com")
                .password(passwordEncoder.encode("12345678"))
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .accountNonExpired(true)
                .role(Role.ADMIN)
                .organization(organization)
                .build();


        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        when(passwordEncoder.matches(any(), any())).thenReturn(true);

        doNothing().when(userDetailsChecker).check(any());

        when(jwtService.generateToken(any())).thenReturn("token");


        LoginRequestDto requestDto = LoginRequestDto.builder()
                .email("eles.elvin@gmail.com")
                .password("12345678")
                .build();

        LoginResponseDto loginResponseDto = userService.signIn(requestDto);

        assertThat(loginResponseDto).isNotNull();
    }

    @Test
    void whenCreateUserThenSuccess() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        UserAuth principal = UserAuth.builder()
                .id(1L)
                .email("eles.elvin@gmail.com")
                .orgId(1L)
                .build();

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(principal);
        SecurityContextHolder.setContext(securityContext);

        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(principal);

        Organization organization = Organization.builder()
                .id(1L)
                .name("organization")
                .phone("+994558479995")
                .address("baku")
                .build();


        when(organizationRepository.findById(anyLong())).thenReturn(Optional.ofNullable(organization));



        User user = User.builder()
                .name("user")
                .email("eles.elvin@gmail.com")
                .password(passwordEncoder.encode("12345678"))
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .accountNonExpired(true)
                .role(Role.USER)
                .build();

        when(userRepository.save(any())).thenReturn(user);

        RegisterDto registerDto = RegisterDto.builder()
                .name("user")
                .email("eles.elvin@gmail.com")
                .password(passwordEncoder.encode("12345678"))
                .build();

        UserDto serviceUser = userService.createUser(registerDto);

        verify(userRepository, times(1)).findByEmail(anyString());
        verify(userRepository, times(1)).save(any());

        assertThat(serviceUser.email()).isEqualTo("eles.elvin@gmail.com");

    }


    @Test
    void whenAssignTaskToUserThenSuccess() {

        Organization organization = Organization.builder()
                .id(1L)
                .name("organization")
                .phone("+994558479995")
                .address("baku")
                .build();

        Task task = Task.builder()
                .id(1L)
                .title("task1")
                .description("task1 description")
                .deadline(LocalDateTime.now().plusDays(2))
                .status("Pending")
                .organization(organization)
                .build();


        User user = User.builder()
                .id(1L)
                .name("user")
                .email("eles.elvin@gmail.com")
                .password(passwordEncoder.encode("12345678"))
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .accountNonExpired(true)
                .role(Role.USER)
                .task(task)
                .organization(organization)
                .build();


        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));


        when(userRepository.assiginTask(anyLong(),anyLong())).thenReturn(1);

        doNothing().when(emailService).sendMail(anyString(), anyString());

        userService.assignTask(task, 1L);

        verify(userRepository, times(1)).findById(anyLong());

    }
}













