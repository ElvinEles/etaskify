package com.etaskify.service;

import com.etaskify.dto.*;
import com.etaskify.mapper.OrgMapper;
import com.etaskify.mapper.TaskMapper;
import com.etaskify.model.Organization;
import com.etaskify.model.Task;
import com.etaskify.repository.OrganizationRepository;
import com.etaskify.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    @InjectMocks
    private TaskService taskService;
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserService userService;
    @Mock
    private OrganizationRepository organizationRepository;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Authentication authentication;

    @Test
    void whenCreateTaskThenSuccess() {

        Organization organization = Organization.builder()
                .id(1L)
                .name("organization")
                .phone("+994558479995")
                .address("baku")
                .build();

        TaskCreateDto dto = TaskCreateDto.builder()
                .title("task1")
                .description("task1 description")
                .deadline("2022-10-07 22:00")
                .status("Pending")
                .build();

        UserAuth principal = UserAuth.builder()
                .id(1L)
                .email("eles.elvin@gmail.com")
                .orgId(1L)
                .build();

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(principal);
        SecurityContextHolder.setContext(securityContext);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        Task task = Task.builder()
                .id(1L)
                .title(dto.title().trim())
                .description(dto.description().trim())
                .status(dto.status())
                .deadline(LocalDateTime.parse("2022-10-07 22:00", formatter))
                .build();

        TaskDto taskdto = TaskDto.builder()
                .id(1L)
                .title(dto.title().trim())
                .description(dto.description().trim())
                .status(dto.status())
                .deadline("2022-10-07 22:00")
                .build();


        OrganizationDto organizationDto = OrganizationDto.builder()
                .id(1L)
                .name("organization")
                .phone("+994558479995")
                .address("baku")
                .build();

        when(organizationRepository.findById(anyLong())).thenReturn(Optional.ofNullable(organization));

        when(taskRepository.save(any())).thenReturn(task);


        TaskDto savedTask = taskService.createTask(dto);

        assertThat(savedTask.id()).isEqualTo(1L);

    }


    @Test
    void whenAssignTaskToUserThenSuccess() {
        Task task = Task.builder()
                .id(1L)
                .title("task1")
                .description("task1 description")
                .deadline(LocalDateTime.now().plusDays(2))
                .status("Pending")
                .build();

        when(taskRepository.findById(anyLong())).thenReturn(Optional.of(task));

        doNothing().when(userService).assignTask(any(), anyLong());

        taskService.assign(1L, 1L);

        verify(taskRepository, times(1)).findById(anyLong());
        verify(userService, times(1)).assignTask(any(), anyLong());
    }

    @Test
    void whenGetAllTasksThenSuccess() {

        UserAuth principal = UserAuth.builder()
                .id(1L)
                .email("eles.elvin@gmail.com")
                .orgId(1L)
                .build();

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(principal);
        SecurityContextHolder.setContext(securityContext);

        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(principal);

        Organization organization = Organization.builder()
                .id(2L)
                .name("organization2")
                .address("baku")
                .phone("+99455")
                .build();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        Task task = Task.builder()
                .id(1L)
                .title("task3")
                .description("task3 description")
                .status("Pending")
                .deadline(LocalDateTime.parse("2022-10-07 22:00", formatter))
                .organization(organization)
                .build();


        TaskDtoAll taskDto = TaskDtoAll.builder()
                .id(1L)
                .title("task3")
                .description("task3 description")
                .status("Pending")
                .deadline("2022-10-07 22:00")
                .build();


        when(taskRepository.findAllTaskWithUser(anyLong())).thenReturn(List.of(task));

//        when(anyList().stream()).thenReturn(Stream.of(task));
//
//        when(anyList().stream().map(any()).toList()).thenReturn(List.of(taskDto));

        List<TaskDtoAll> taskServiceAll = taskService.getAll();

        assertThat(taskServiceAll).hasSize(1);

    }
}