package com.etaskify.service;

import com.etaskify.model.Organization;
import com.etaskify.repository.OrganizationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrganizationServiceTest {

    @InjectMocks
    private OrganizationService organizationService;

    @Mock
    private OrganizationRepository organizationRepository;


    @Test
    void whenCreateOrganizationThenSuccess() {

        Organization organization = Organization.builder()
                .id(1L)
                .name("organization")
                .phone("+994558479995")
                .address("baku")
                .build();


        when(organizationRepository.findByName(anyString())).thenReturn(Optional.empty());

        when(organizationRepository.save(any())).thenReturn(organization);

        Organization organiz = organizationService.create(organization);

        verify(organizationRepository, times(1)).save(any());

        assertThat(organiz.getName()).isEqualTo("organization");
    }


}