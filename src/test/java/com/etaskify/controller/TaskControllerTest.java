package com.etaskify.controller;

import com.etaskify.dto.TaskCreateDto;
import com.etaskify.dto.TaskDto;
import com.etaskify.dto.TaskDtoAll;
import com.etaskify.model.Task;
import com.etaskify.service.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class TaskControllerTest {

    @Mock
    private TaskService taskService;

    private MockMvc mockMvc;

    @InjectMocks
    private ObjectMapper objectMapper;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new TaskController(taskService)).build();
    }


    @Test
    void whenCreateTaskThenSuccess() throws Exception {
        TaskCreateDto createDto = TaskCreateDto.builder()
                .title("task1")
                .description("task1 description")
                .deadline("2022-10-07 22:00")
                .status("Pending")
                .build();


        TaskDto taskDto = TaskDto.builder()
                .id(1L)
                .title("task1")
                .description("task1 description")
                .deadline("2022-10-07 22:00")
                .status("Pending")
                .build();

        when(taskService.createTask(any())).thenReturn(taskDto);

        mockMvc.perform(post("/api/v1/task")
                        .content(objectMapper.writeValueAsString(createDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    void whenAssignTaskToUserThenSuccess() throws Exception {

        doNothing().when(taskService).assign(anyLong(),anyLong());


        mockMvc.perform(post("/api/v1/task/assign/task/1/user/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetAllTasksThenSuccess() throws Exception {

        TaskDtoAll taskDtoall = TaskDtoAll.builder()
                .id(1L)
                .title("task3")
                .description("task3 description")
                .status("Pending")
                .deadline("2022-10-07 22:00")
                .build();

        when(taskService.getAll()).thenReturn(List.of(taskDtoall));

        mockMvc.perform(get("/api/v1/task"))
                .andExpect(status().isOk());
    }

}















