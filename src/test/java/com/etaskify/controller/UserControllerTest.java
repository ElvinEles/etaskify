package com.etaskify.controller;

import com.etaskify.dto.*;
import com.etaskify.model.User;
import com.etaskify.service.UserService;
import com.etaskify.validation.execution.PhoneNumberValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    private UserService userService;

    private MockMvc mockMvc;

    @InjectMocks
    private ObjectMapper objectMapper;

    @Mock
    private PhoneNumberValidator phoneNumberValidator;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new UserController(userService)).build();
    }


    @Test
    void whenSignUpThenSuccess() throws Exception {

        SignUpDto signUpDto = SignUpDto.builder()
                .nameOrg("organization")
                .phone("+994775404544")
                .address("Baku")
                .username("james")
                .email("eles.elvin@gmail.com")
                .password("123456789")
                .build();

        doNothing().when(userService).signUp(any());

          when(phoneNumberValidator.isValid(any(),any())).thenReturn(true);

        mockMvc.perform(post("/api/v1/user/permit/signup")
                        .content(objectMapper.writeValueAsString(signUpDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    void whenSignInThenSuccess() throws Exception {

        LoginRequestDto requestDto = LoginRequestDto.builder()
                .email("eles.elvin@gmail.com")
                .password("12345678")
                .build();

        LoginResponseDto responseDto = LoginResponseDto.builder()
                .jwt("token")
                .build();

        when(userService.signIn(any())).thenReturn(responseDto);


        mockMvc.perform(post("/api/v1/user/permit/signin")
                        .content(objectMapper.writeValueAsString(requestDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.jwt").value("token"));

    }

    @Test
    void whenCreateUserThenSuccess() throws Exception {
        UserDto userDto= UserDto.builder()
                .email("eles.elvin@gmail.com")
                .name("elvin")
                .surname("eles")
                .build();

        when(userService.createUser(any())).thenReturn(userDto);

        RegisterDto dto = RegisterDto.builder()
                .email("eles.elvin@gmail.com")
                .name("elvin")
                .surname("eles")
                .password("12345678")
                .build();

        mockMvc.perform(post("/api/v1/user/auth/create")
                        .content(objectMapper.writeValueAsString(dto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}















